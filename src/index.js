import React from 'react';
import ReactDOM from 'react-dom';

import Counter from './components/counter';

import {createStore, bindActionCreators} from 'redux';

import { Provider } from 'react-redux';
import App from './components/app';




import reducer from './reducer';
import * as actions from './actions';






const store = createStore(reducer);




  ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider> , document.getElementById('root'));








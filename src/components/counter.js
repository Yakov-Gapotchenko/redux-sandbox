import React from 'react';
import {bindActionCreators} from 'redux';
import { connect } from 'react-redux';
import * as actions from '../actions';


const Counter = ({ counter , inc, dec, rnd }) => {
    return (
        <div className="jumbotron">
      <h2 id="counter"> { counter } </h2>
      <button 
      onClick={dec} 
      class="btn btn-primary btn-lg"> DEC </button>
      <button 
      onClick={inc} 
      class="btn btn-primary btn-lg"> INC </button>
      <button 
      onClick={rnd}  
      class="btn btn-primary btn-lg"> RND </button>
    </div>
    );

};



const mapStateToProps = (state) => {
  return {
    counter: state

  };
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(actions, dispatch);
};



export default connect(mapStateToProps, actions)(Counter);




